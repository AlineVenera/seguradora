package com.seguradora.entidade;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Mapeamento de ve�culo
 * @author juliano.vieira
 * @since 03/05/2017
 */

@Entity
@Table(name="veiculo")
public class Veiculo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "cdMarca")
	private Marca marca;
	
	@Column(name = "descricaoModelo")
	private String descricaoModelo;
	
	@Column(name = "anoFabricacao")
	private Integer anoFabricacao;
	
	@Column(name = "anoModelo")
	private Integer anoModelo;
	
	@Column(name = "valorFipe")
	private BigDecimal valorFipe;
	
	
	public Veiculo() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public String getDescricaoModelo() {
		return descricaoModelo;
	}

	public void setDescricaoModelo(String descricaoModelo) {
		this.descricaoModelo = descricaoModelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}
	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}
	public Integer getAnoModelo() {
		return anoModelo;
	}
	public void setAnoModelo(Integer anoModelo) {
		this.anoModelo = anoModelo;
	}

	public BigDecimal getValorFipe() {
		return valorFipe;
	}

	public void setValorFipe(BigDecimal valorFipe) {
		this.valorFipe = valorFipe;
	}

	
}