package com.seguradora.types;

/**
 * Tipos de resultado para as cota��es
 * @author aline.venera
 * @since 01/05/2017
 */

public enum TipoResultadoCotacao {
	
	TOTAL ("Seguro total"),
	ROUBO_TERCEIRO ("Seguro contra roubo, furto e terceiros"),
	TERCEIRO ("Seguro contra terceiros");
	
	private String label;
	
	private TipoResultadoCotacao(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
