package com.seguradora.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.seguradora.entidade.Usuario;
import com.seguradora.util.HibernateUtil;

public class UsuarioDao {
	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();

	public boolean validarUsuario(String nome, String senha) {
		Usuario usuario;

		Session session = this.sessionFactory.openSession();
		Query q = session.createQuery("from usuario where nome = :nome and senha = :senha");

		q.setString("nome", nome);
		q.setString("senha", senha);

		usuario = (Usuario) q.uniqueResult();
		session.close();

		if (usuario != null){
			return true;
		}
		return false;
	}

	public Boolean salvarUsuario(String nome, int idade, String senha) {
		Usuario usuario = new Usuario();
		usuario.setIdade(idade);
		usuario.setNome(nome);
		usuario.setSenha(senha);

		try{
			Session session = this.sessionFactory.openSession();
			session.beginTransaction();
			session.save(usuario);
			session.getTransaction().commit();
			session.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
