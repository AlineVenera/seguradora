package com.seguradora.dao;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.seguradora.entidade.Usuario;
import com.seguradora.util.HibernateUtil;

public class UsuarioDao {

	public boolean validarUsuario(String nome, String senha) {
		EntityManager em = HibernateUtil.getManager();
		Usuario usuario;

		TypedQuery<Usuario> q = em.createQuery("from Usuario where nome = :nome and senha = :senha", Usuario.class);

		q.setParameter("nome", nome);
		q.setParameter("senha", senha);

		usuario = (Usuario) q.getSingleResult();
		HibernateUtil.closeManager(em);

		if (usuario != null){
			return true;
		}
		return false;
	}

	public Boolean salvarUsuario(String nome, int idade, String senha) {
		Usuario usuario = new Usuario();
		usuario.setIdade(idade);
		usuario.setNome(nome);
		usuario.setSenha(senha);

		try{
			EntityManager em = HibernateUtil.getManager();
			em.getTransaction().begin();
			em.persist(usuario);
			em.getTransaction().commit();
			HibernateUtil.closeManager(em);
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
