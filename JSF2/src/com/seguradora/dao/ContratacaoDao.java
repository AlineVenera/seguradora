package com.seguradora.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.seguradora.entidade.Contratacao;
import com.seguradora.util.HibernateUtil;

public class ContratacaoDao {

	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();
	
	/**
	 * Salva a contratacao
	 * @param cotacao
	 * @return
	 */
	public Boolean salva(Contratacao contratacao) {
		try{
			Session session = this.sessionFactory.openSession();
			session.beginTransaction();
			session.save(contratacao);
			session.getTransaction().commit();
			session.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
