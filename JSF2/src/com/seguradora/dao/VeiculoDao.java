package com.seguradora.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.seguradora.entidade.Veiculo;
import com.seguradora.util.HibernateUtil;

public class VeiculoDao {

	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();
	
	/**
	 * Salva o ve�culo que for cotado
	 * @param veiculo
	 * @return
	 */
	public Boolean salvarVeiculo(Veiculo veiculo) {
		try{
			Session session = this.sessionFactory.openSession();
			session.beginTransaction();
			session.save(veiculo);
			session.getTransaction().commit();
			session.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
