package com.seguradora.dao;

import javax.persistence.EntityManager;

import com.seguradora.entidade.Veiculo;
import com.seguradora.util.HibernateUtil;

public class VeiculoDao {

	/**
	 * Salva o ve�culo que for cotado
	 * @param veiculo
	 * @return
	 */
	public Boolean salvarVeiculo(Veiculo veiculo) {
		try{
			EntityManager em = HibernateUtil.getManager();
			em.getTransaction().begin();
			em.persist(veiculo);
			em.getTransaction().commit();
			HibernateUtil.closeManager(em);
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
