package com.seguradora.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.seguradora.entidade.Cotacao;
import com.seguradora.util.HibernateUtil;

public class CotacaoDao {

	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();
	
	/**
	 * Salva os valores da cota��o feita
	 * @param cotacao
	 * @return
	 */
	public Boolean salva(Cotacao cotacao) {
		try{
			Session session = this.sessionFactory.openSession();
			session.beginTransaction();
			session.save(cotacao);
			session.getTransaction().commit();
			session.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
