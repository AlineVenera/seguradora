package com.seguradora.dao;

import javax.persistence.EntityManager;

import com.seguradora.entidade.Cotacao;
import com.seguradora.util.HibernateUtil;

public class CotacaoDao {

	/**
	 * Salva os valores da cota��o feita
	 * @param cotacao
	 * @return
	 */
	public Boolean salva(Cotacao cotacao) {
		try{
			EntityManager em = HibernateUtil.getManager();
			em.getTransaction().begin();
			em.persist(cotacao);
			em.getTransaction().commit();
			HibernateUtil.closeManager(em);
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
