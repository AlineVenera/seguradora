package com.seguradora.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.seguradora.entidade.Perfil;
import com.seguradora.util.HibernateUtil;

public class PerfilDao {

	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();
	
	/**
	 * Salva o perfil do cliente
	 * @param perfil
	 * @return
	 */
	public Boolean salvaPerfil(Perfil perfil) {
		try{
			Session session = this.sessionFactory.openSession();
			session.beginTransaction();
			session.save(perfil);
			session.getTransaction().commit();
			session.close();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
