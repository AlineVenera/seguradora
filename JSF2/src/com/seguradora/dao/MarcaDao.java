package com.seguradora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.seguradora.entidade.Marca;
import com.seguradora.util.HibernateUtil;

public class MarcaDao {

	public List<Marca> get(String condicao) {
		EntityManager em = HibernateUtil.getManager();
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select m from Marca m ");
		sql.append(" where 1=1 ");
		if(!condicao.isEmpty()){
			sql.append(" and upper(nome) like concat('%', upper(:nome), '%') ");
		}
		sql.append(" order by 1 ");
		TypedQuery<Marca> query = em.createQuery(sql.toString(), Marca.class);
		if(!condicao.isEmpty()){
			query.setParameter("condicao", condicao);
		}
		HibernateUtil.closeManager(em);
		return query.getResultList();
	}
}
