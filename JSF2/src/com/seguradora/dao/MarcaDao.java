package com.seguradora.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.seguradora.entidade.Marca;
import com.seguradora.util.HibernateUtil;

public class MarcaDao {

	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();
	
	public List<Marca> get(String condicao) {
		Session session = this.sessionFactory.openSession();
				
		StringBuilder sql = new StringBuilder();
		sql.append(" select m from Marca m ");
		sql.append(" where 1=1 ");
		if(!condicao.isEmpty()){
			sql.append(" and upper(nome) like concat('%', upper(:nome), '%') ");
		}
		sql.append(" order by 1 ");
		Query query = session.createQuery(sql.toString());
		if(!condicao.isEmpty()){
			query.setParameter("condicao", condicao);
		}
		return query.list();
	}
}
