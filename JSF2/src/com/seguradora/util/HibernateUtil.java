package com.seguradora.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {

	public static EntityManager getManager() {
        try {
        	EntityManagerFactory factory = Persistence.createEntityManagerFactory("JSF2");
        	EntityManager manager = factory.createEntityManager();
        	manager.getTransaction().begin();
        	return manager;
        }catch (Throwable ex) {
            System.err.println("Initial Entity manager failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
	
	public static void closeManager(EntityManager manager) {
        try {
        	manager.flush();
        	manager.close();
        }catch (Throwable ex) {
            System.err.println("Initial Entity manager failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
