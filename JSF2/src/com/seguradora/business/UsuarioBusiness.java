package com.seguradora.business;

import com.seguradora.dao.UsuarioDao;
import com.seguradora.entidade.Usuario;

public class UsuarioBusiness {
	
	/**
	 * Salva o usu�rio
	 * Se salvar retorna true, se n�o salvar retorna false
	 * @param usuario
	 * @return
	 */
	public boolean salvar(Usuario usuario){
		if(usuario == null){
			return false;
		}
		UsuarioDao usuarioDao = new UsuarioDao();
		//Se der algum erro dentro do m�todo salvar, retorna false. Se correr tudo bem, suceeeeessooo
		return usuarioDao.salvarUsuario(usuario.getNome(), usuario.getIdade(), usuario.getSenha());
	}
	
}
