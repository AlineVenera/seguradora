package com.seguradora.business;

import java.math.BigDecimal;
import java.util.Calendar;

import com.seguradora.dao.CotacaoDao;
import com.seguradora.entidade.Cotacao;
import com.seguradora.types.TipoResultadoCotacao;

public class CotacaoBusiness {
	
	/**
	 * Calcula a cota��o
	 * @param tipoResultadoCotacao, anoCarro, valorFipe
	 * @return
	 */
	public BigDecimal calculaCotacao(TipoResultadoCotacao tipoResultadoCotacao, int anoCarro, BigDecimal valorFipe){
		boolean carroTemMaisTresAnos = (Calendar.getInstance().get(Calendar.YEAR) - anoCarro) >= 3 ;
		BigDecimal taxaAplicada = new BigDecimal("0");
		
		switch (tipoResultadoCotacao) {
		case TOTAL:
			if(carroTemMaisTresAnos){
				taxaAplicada = new BigDecimal(0.5);
			}
			taxaAplicada = new BigDecimal(0.65);
			break;
		case ROUBO_TERCEIRO:
			if(carroTemMaisTresAnos){
				taxaAplicada = new BigDecimal(0.3);
			}
			taxaAplicada = new BigDecimal(0.35);
			break;
		case TERCEIRO:
			if(carroTemMaisTresAnos){
				taxaAplicada = new BigDecimal(0.1);
			}
			taxaAplicada = new BigDecimal(0.2);
		break;
		default:
			break;
		}
		return taxaAplicada.multiply(valorFipe);
	}
	
	/**
	 * Salvar a cota��o
	 * @param cotacao
	 * @return
	 */
	public boolean salvar(Cotacao cotacao){
		CotacaoDao cotacaoDao = new CotacaoDao();
		return cotacaoDao.salva(cotacao);
	}
	
}
