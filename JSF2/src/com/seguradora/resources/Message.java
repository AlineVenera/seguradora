package com.seguradora.resources;

import javax.faces.bean.ManagedBean;

@ManagedBean(name="message",eager=true)
public class Message {
	private String message = "Hello World";
	private String message2 = "Hello World!!!!!!!!!!!!!!";

	public String getMessage() {
		return message;
	}

	public String getMessage2() {
		return message2;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
