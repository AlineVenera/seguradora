package com.seguradora.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import com.seguradora.business.UsuarioBusiness;
import com.seguradora.dao.UsuarioDao;
import com.seguradora.entidade.Usuario;

@ManagedBean(name="loginMBean")
public class LoginMBean {
	
	private Usuario usuario;
	private UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
	
	@PostConstruct
	public void init(){
		usuario = new Usuario();
	}
	
	public boolean logar(){
		if(getUsuario() == null || getUsuario().getNome().isEmpty())
			System.out.println("Error----------------");
		
		UsuarioDao usuarioDao = new UsuarioDao();
		if (usuarioDao.validarUsuario(usuario.getNome(), usuario.getSenha()))
			return true;
		
		return false;
	}
	
	public void salvar(){
		usuarioBusiness.salvar(getUsuario());
	}
	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
