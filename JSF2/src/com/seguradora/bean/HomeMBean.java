package com.seguradora.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import com.seguradora.business.ContratacaoBusiness;
import com.seguradora.business.CotacaoBusiness;
import com.seguradora.entidade.Contratacao;
import com.seguradora.entidade.Cotacao;
import com.seguradora.entidade.Marca;
import com.seguradora.entidade.Perfil;
import com.seguradora.entidade.Veiculo;
import com.seguradora.types.TipoResultadoCotacao;

@ManagedBean(name="homeMBean")
public class HomeMBean {
	
	private String marca;
	private List<Marca> marcas;
	private Veiculo veiculo;
	private Perfil perfil;
	private Cotacao cotacao;
	private Contratacao contratacao;
	
	private CotacaoBusiness cotacaoBusiness = new CotacaoBusiness();
	private ContratacaoBusiness contratacaoBusiness = new ContratacaoBusiness();
	
	@PostConstruct
	public void init(){
		setMarca(null);
		setMarcas(new ArrayList<Marca>());
		setVeiculo(new Veiculo());
	}
	
	
	public List<Map.Entry<TipoResultadoCotacao, BigDecimal>> getResultadoCotacao(){
		Map<TipoResultadoCotacao, BigDecimal> map = new HashMap<TipoResultadoCotacao, BigDecimal>();
		for(TipoResultadoCotacao tipoResultado : TipoResultadoCotacao.values()){
			map.put(tipoResultado, cotacaoBusiness.calculaCotacao(tipoResultado, getVeiculo().getAnoFabricacao(), getVeiculo().getValorFipe()));
		}
		
		Set<Map.Entry<TipoResultadoCotacao, BigDecimal>> set = map.entrySet();
		return new ArrayList<Map.Entry<TipoResultadoCotacao, BigDecimal>>(set);
	}
	
	public void contratar(){
		contratacaoBusiness.contratar(getContratacao());
	}
	
	public Cotacao getCotacao() {
		return cotacao;
	}

	public void setCotacao(Cotacao cotacao) {
		this.cotacao = cotacao;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public List<Marca> getMarcas() {
		return marcas;
	}


	public void setMarcas(List<Marca> marcas) {
		this.marcas = marcas;
	}


	public Veiculo getVeiculo() {
		return veiculo;
	}


	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}


	public Perfil getPerfil() {
		return perfil;
	}


	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}


	public Contratacao getContratacao() {
		return contratacao;
	}


	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}

	
	
}
