package com.seguradora.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import com.seguradora.business.ContratacaoBusiness;
import com.seguradora.business.CotacaoBusiness;
import com.seguradora.entidade.Contratacao;
import com.seguradora.entidade.Cotacao;
import com.seguradora.entidade.Marca;
import com.seguradora.entidade.Perfil;
import com.seguradora.entidade.Veiculo;
import com.seguradora.types.TipoResultadoCotacao;

@ManagedBean(name="homeMBean")
@SessionScoped
public class HomeMBean {
	
	private String marca;
	private List<Marca> marcas;
	private Veiculo veiculo;
	private Perfil perfil;
	private Cotacao cotacao;
	private Contratacao contratacao;
	
	private CotacaoBusiness cotacaoBusiness = new CotacaoBusiness();
	private ContratacaoBusiness contratacaoBusiness = new ContratacaoBusiness();
	
	@PostConstruct
	public void init(){
		setMarca("");
		setMarcas(new ArrayList<Marca>());
		setVeiculo(new Veiculo());
		setPerfil(new Perfil());
	}
	
	
	public List<Map.Entry<TipoResultadoCotacao, BigDecimal>> getResultadoCotacao(){
		Map<TipoResultadoCotacao, BigDecimal> map = new HashMap<TipoResultadoCotacao, BigDecimal>();
		for(TipoResultadoCotacao tipoResultado : TipoResultadoCotacao.values()){
			map.put(tipoResultado, cotacaoBusiness.calculaCotacao(tipoResultado, getVeiculo().getAnoFabricacao(), getVeiculo().getValorFipe()));
		}
		
		Set<Map.Entry<TipoResultadoCotacao, BigDecimal>> set = map.entrySet();
		return new ArrayList<Map.Entry<TipoResultadoCotacao, BigDecimal>>(set);
	}
	
	public void contratar(){
		contratacaoBusiness.contratar(getContratacao());
	}
	
	public void validarAno(FacesContext ctx, UIComponent component, Object value)throws ValidatorException{
		if(null != value && null != value.toString() && !value.toString().isEmpty()){
			String ano = value.toString();
			Integer anoInt;
			try{
				anoInt = Integer.valueOf(ano);
			}catch(NumberFormatException nfe){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ano informado � inv�lido.", null));
			}
			if(anoInt < 2010 && anoInt > 2017){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ano informado � inv�lido. Por favor informe um ano entre 2010 e 2017.", null));
			}
		}
	}
	
	public void validarEmail(FacesContext ctx, UIComponent component, Object value)throws ValidatorException{
		if(null != value && null != value.toString() && !value.toString().isEmpty()){
			String email = value.toString();
			if(email.contains("@") && (email.endsWith(".com") || email.endsWith(".br"))){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail inv�lido. Por favor informe um endere�o de e-mail v�lido.", null));
			}
		}
	}
	
	public Cotacao getCotacao() {
		return cotacao;
	}

	public void setCotacao(Cotacao cotacao) {
		this.cotacao = cotacao;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public List<Marca> getMarcas() {
		return marcas;
	}


	public void setMarcas(List<Marca> marcas) {
		this.marcas = marcas;
	}


	public Veiculo getVeiculo() {
		return veiculo;
	}


	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}


	public Perfil getPerfil() {
		return perfil;
	}


	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}


	public Contratacao getContratacao() {
		return contratacao;
	}


	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}

	
	
}
