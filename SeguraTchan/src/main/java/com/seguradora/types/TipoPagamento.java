package com.seguradora.types;

/**
 * Tipos de pagamentos aceitos
 * @author aline.venera
 * @since 01/05/2017
 */

public enum TipoPagamento {
	
	BOLETO ("Boleto"),
	CARTAO ("Cart�o de cr�dito");
	
	private String label;
	
	private TipoPagamento(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
