package com.seguradora.types;

/**
 * Tipos de ve�culo que podem ser cotados
 * @author aline.venera
 * @since 01/05/2017
 */

public enum TipoVeiculo {
	
	MOTO ("Moto"),
	CARRO ("Carro"),
	CAMINHAO ("Caminh�o");
	
	private String label;
	
	private TipoVeiculo(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
}
