package com.seguradora.entidade;

import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Dados salvos da cota��o
 * @author elaine.santos
 * @since 01/05/2017
 */

@Entity
@Table(name="cotacao")
public class Cotacao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "cdPerfil")
	private Perfil perfil;
	
	@ManyToOne
	@JoinColumn(name = "cdVeiculo")
	private Veiculo veiculo;
	
	@Column(name = "valor")
	private BigDecimal valorCotacao;
	
	@Column(name = "data_validade")
	private Date dataValidade;
	
	public Cotacao() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public BigDecimal getValorCotacao() {
		return valorCotacao;
	}
	public void setValorCotacao(BigDecimal valorCotacao) {
		this.valorCotacao = valorCotacao;
	}
	public Date getDataValidade() {
		return dataValidade;
	}
	public void setDataValidade(Date dataValidade) {
		this.dataValidade = dataValidade;
	}
}