package com.seguradora.dao;

import javax.persistence.EntityManager;

import com.seguradora.entidade.Perfil;
import com.seguradora.util.HibernateUtil;

public class PerfilDao {

	/**
	 * Salva o perfil do cliente
	 * @param perfil
	 * @return
	 */
	public Boolean salvaPerfil(Perfil perfil) {
		try{
			EntityManager em = HibernateUtil.JpaEntityManager();
			em.getTransaction().begin();
			em.persist(perfil);
			em.getTransaction().commit();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
