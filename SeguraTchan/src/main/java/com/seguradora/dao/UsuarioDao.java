package com.seguradora.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import com.seguradora.entidade.Usuario;
import com.seguradora.util.HibernateUtil;

public class UsuarioDao {

	/**
	 * Validar o usu�rio 
	 * @param nome
	 * @param senha
	 * @return
	 */
	public boolean validarUsuario(String nome, String senha) {
		EntityManager em = HibernateUtil.JpaEntityManager();
		Usuario usuario;

		TypedQuery<Usuario> q = em.createQuery("from Usuario where nome = :nome and senha = :senha", Usuario.class);

		q.setParameter("nome", nome);
		q.setParameter("senha", senha);

		try{
			usuario = (Usuario) q.getSingleResult();
		}catch (NoResultException nre) {
			return false;
		}
		
		if (usuario != null){
			return true;
		}
		return false;
	}

	/**
	 * Salvar um usu�rio
	 * @param usuario
	 * @return
	 */
	public Boolean salvarUsuario(Usuario usuario) {
		try{
			EntityManager em = HibernateUtil.JpaEntityManager();
			if(null == usuario.getId()){//Se n�o tiver ID ainda, ent�o salva
				em.persist(usuario);
			}else{//Se j� tiver ID, ent�o atualiza o registro
				em.merge(usuario);
			}
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * Recupera lista com todos os usu�rios
	 * @return
	 */
	public List<Usuario> getUsuarios() {
		try{
			EntityManager em = HibernateUtil.JpaEntityManager();
			TypedQuery<Usuario> q = em.createQuery("from Usuario", Usuario.class);

			return (List<Usuario>) q.getResultList();
		}catch(Exception e){
			return new ArrayList<>();
		}
	}
	
	/**
	 * Exclui um usu�rio
	 * @param usuario
	 * @return
	 */
	public boolean excluir(Usuario usuario){
		try{
			EntityManager em = HibernateUtil.JpaEntityManager();
			em.remove(usuario);
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
