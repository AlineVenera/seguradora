package com.seguradora.dao;

import javax.persistence.EntityManager;

import com.seguradora.entidade.Contratacao;
import com.seguradora.util.HibernateUtil;

public class ContratacaoDao {

	/**
	 * Salva a contratacao
	 * @param cotacao
	 * @return
	 */
	public Boolean salva(Contratacao contratacao) {
		try{
			EntityManager em = HibernateUtil.JpaEntityManager();
			em.getTransaction().begin();
			em.persist(contratacao);
			em.getTransaction().commit();
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
