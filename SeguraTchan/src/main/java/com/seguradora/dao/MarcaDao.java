package com.seguradora.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.seguradora.entidade.Marca;
import com.seguradora.util.HibernateUtil;

public class MarcaDao {

	/**
	 * Recupera uma lista com todas as marcas gravadas no banco
	 * @return
	 */
	public List<Marca> get() {
		EntityManager em = HibernateUtil.JpaEntityManager();
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select m from Marca m ");
		sql.append(" order by 1 ");
		TypedQuery<Marca> query = em.createQuery(sql.toString(), Marca.class);
		return query.getResultList();
	}
}
