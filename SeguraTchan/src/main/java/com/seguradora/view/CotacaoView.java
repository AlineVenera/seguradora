package com.seguradora.view;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.primefaces.event.SelectEvent;

import com.seguradora.business.ContratacaoBusiness;
import com.seguradora.business.CotacaoBusiness;
import com.seguradora.entidade.Contratacao;
import com.seguradora.entidade.Cotacao;
import com.seguradora.entidade.Marca;
import com.seguradora.entidade.Perfil;
import com.seguradora.entidade.Veiculo;
import com.seguradora.types.TipoResultadoCotacao;

@ManagedBean(name="cotacaoView")
@SessionScoped
public class CotacaoView {
	
	private Long marca;
	private Veiculo veiculo;
	private Perfil perfil;
	private Cotacao cotacao;
	private Contratacao contratacao;
	private String tipoResultadoSelecionado;
	private BigDecimal valorCotacao;
	
	private CotacaoBusiness cotacaoBusiness = new CotacaoBusiness();
	private ContratacaoBusiness contratacaoBusiness = new ContratacaoBusiness();
	
	/**
	 * M�todo quando inicia a tela. Serve pra iniciar as vari�veis
	 */
	@PostConstruct
	public void init(){
		setMarca(null);
		setVeiculo(new Veiculo());
		setPerfil(new Perfil());
		setCotacao(new Cotacao());
	}
	
	/**
	 * Esse m�todo � pra transformar o map de resultado da cota��o em uma lista, pra ser usada na tela
	 * @return
	 */
	public List<Map.Entry<TipoResultadoCotacao, BigDecimal>> getResultadoCotacao(){
		Map<TipoResultadoCotacao, BigDecimal> map = new HashMap<TipoResultadoCotacao, BigDecimal>();
		//Aqui percorre os tipos de resultado
		for(TipoResultadoCotacao tipoResultado : TipoResultadoCotacao.values()){
			//Calcula o valor da cota��o de acordo com o tipo e com o ano do carro
			BigDecimal valorTipo = cotacaoBusiness.calculaCotacao(tipoResultado, getVeiculo().getAnoFabricacao(), getVeiculo().getValorFipe());
			//Adiciona ao MAP
			map.put(tipoResultado, valorTipo);
		}
		//Aqui ele converte pra lista e retorna
		Set<Map.Entry<TipoResultadoCotacao, BigDecimal>> set = map.entrySet();
		return new ArrayList<Map.Entry<TipoResultadoCotacao, BigDecimal>>(set);
	}
	
	public void contratar(){
		contratacaoBusiness.contratar(getContratacao());
	}
	
	/**
	 * Validar o ano do carro, tem que ser um n�mero inteiro e entre 2010 e 2017
	 * @param ctx
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 */
	public void validarAno(FacesContext ctx, UIComponent component, Object value)throws ValidatorException{
		if(null != value && null != value.toString() && !value.toString().isEmpty()){
			String ano = value.toString();
			Integer anoInt;
			try{
				anoInt = Integer.valueOf(ano);
			}catch(NumberFormatException nfe){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ano informado � inv�lido.", null));
			}
			if(anoInt < 2010 && anoInt > 2017){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ano informado � inv�lido. Por favor informe um ano entre 2010 e 2017.", null));
			}
		}
	}
	
	/**
	 * M�todo para a valida��o de email. S� � v�lido se tiver @ e terminar com ".com" ou ".br"
	 * @param ctx
	 * @param component
	 * @param value
	 * @throws ValidatorException
	 */
	public void validarEmail(FacesContext ctx, UIComponent component, Object value)throws ValidatorException{
		if(null != value && null != value.toString() && !value.toString().isEmpty()){
			String email = value.toString().trim();
			if(!email.contains("@") || !(email.endsWith(".com") || email.endsWith(".br"))){
				throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail inv�lido. Por favor informe um endere�o de e-mail v�lido.", null));
			}
		}
	}
	
	 /**
	  * M�todo que recupera a lista de Marcas pra ser usada na tela 
	  * @return
	  */
	public List<SelectItem> marcas(){
		List<Marca> marcas =cotacaoBusiness.getMarcas();
		List<SelectItem> itens = new ArrayList<>();
		for(Marca marca : marcas){
			itens.add(new SelectItem(marca.getId(), marca.getNome()));
		}
		return itens;
	}
	
	/**
	 * Esse m�todo � acionado quando a linha na tabela de resultados da cota��o � selecionada, 
	 * pra gravar o perfil, ve�culo e cota��o selecionados
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		String tipo = event.getObject().toString().substring(0, event.getObject().toString().indexOf("="));
		TipoResultadoCotacao tipoResultado = TipoResultadoCotacao.valueOf(tipo);
		getCotacao().setVeiculo(getVeiculo());
		getCotacao().setPerfil(getPerfil());
		getCotacao().setValorCotacao(cotacaoBusiness.calculaCotacao(tipoResultado, getVeiculo().getAnoFabricacao(), getVeiculo().getValorFipe()));
		cotacaoBusiness.salvar(getCotacao());
    }
	
	public void finalizar() throws IOException{
		ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext();
		ex.redirect("/salvou.xhtml");
	}
 
	
	public Cotacao getCotacao() {
		return cotacao;
	}

	public void setCotacao(Cotacao cotacao) {
		this.cotacao = cotacao;
	}

	public Long getMarca() {
		return marca;
	}

	public void setMarca(Long marca) {
		this.marca = marca;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Contratacao getContratacao() {
		return contratacao;
	}

	public void setContratacao(Contratacao contratacao) {
		this.contratacao = contratacao;
	}

	public String getTipoResultadoSelecionado() {
		return tipoResultadoSelecionado;
	}

	public void setTipoResultadoSelecionado(String tipoResultadoSelecionado) {
		this.tipoResultadoSelecionado = tipoResultadoSelecionado;
	}

	public BigDecimal getValorCotacao() {
		return valorCotacao;
	}

	public void setValorCotacao(BigDecimal valorCotacao) {
		this.valorCotacao = valorCotacao;
	}
}
