package com.seguradora.view;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.seguradora.business.UsuarioBusiness;
import com.seguradora.entidade.Usuario;

@ManagedBean(name="loginView")
@SessionScoped
public class LoginView {
	
	private Usuario usuario;
	private UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
	
	@PostConstruct
	public void init(){
		usuario = new Usuario();
	}
	
	/**
	 * M�todo respons�vel pelo login
	 * @throws IOException
	 */
	public void logar() throws IOException{
		if(getUsuario().getNome() == null || getUsuario().getNome().isEmpty()
				|| getUsuario().getSenha() == null || getUsuario().getSenha().isEmpty()){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "", "Informe seu usu�rio e senha"));
			return;
		}
		
		if (usuarioBusiness.validarUsuario(getUsuario().getNome(), getUsuario().getSenha())){
			ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext(); 
			ex.redirect("home.xhtml");
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "", "Usu�rio ou senha inv�lido"));
		}
	}
	
	/**
	 * Esqueci a minha senha, direciona pra tela esqueci minha senha
	 * @throws IOException
	 */
	public void esqueciMinhaSenha() throws IOException{
		ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext();
		ex.redirect("esqueciAsenha.xhtml");
	}
	
	/**
	 * Salvar o usu�rio
	 */
	public void salvar(){
		usuarioBusiness.salvar(getUsuario());
	}
	

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
