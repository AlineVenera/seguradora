package com.seguradora.view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.seguradora.business.UsuarioBusiness;
import com.seguradora.entidade.Usuario;

@ManagedBean(name="usuarioView")
@RequestScoped
public class UsuarioView {
	
	private List<Usuario> usuarios;
	private Usuario usuario;
	private UsuarioBusiness usuarioBusiness = new UsuarioBusiness();
	
	@PostConstruct
	public void init(){
		setUsuario(new Usuario());
		setUsuarios(new ArrayList<>());
	}
	
	/**
	 * Carrega uma lista com todos os usu�rios
	 * @throws IOException
	 */
	public void carregarUsuarios() throws IOException{
		setUsuarios(usuarioBusiness.getUsuarios());
	}
	
	/**
	 * M�todo serve para limpar o novo usu�rio.
	 * Vai ser chamado antes de abrir o dialog de criar novo usu�rio
	 * @throws IOException
	 */
	public void novo() throws IOException{
		setUsuario(new Usuario());
	}
	
	/**
	 * Salva o usu�rio
	 * @throws IOException
	 */
	public void salvar() throws IOException{
		if(getUsuario().novaSenhaValida()){
			usuarioBusiness.salvar(getUsuario());
			carregarUsuarios();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Cadastro realizado com sucesso"));
		}
	}
	
	/**
	 * M�todo que vai ser chamado antes de abrir o dialog de trocar senha, 
	 * serve pra adicionar � vari�vel de "usu�rio" o usu�rio que cliquei no bot�o 
	 * @param usuario
	 */
	public void trocarSenha(Usuario usuario){
		setUsuario(usuario);
	}
	
	/**
	 * Trocar a senha do usu�rio
	 */
	public void trocarSenha(){
		//Verifica se a nova senha � v�lida
		if(getUsuario().novaSenhaValida()){
			usuarioBusiness.salvar(getUsuario());
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Senha alterada com sucesso"));
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "", "Para efetuar a troca da senha, as senhas informadas devem ser iguais"));
		}
	}
	
	/**
	 * M�todo que direciona para a home quando clica no bot�o voltar
	 * @throws IOException
	 */
	public void home() throws IOException{
		ExternalContext ex = FacesContext.getCurrentInstance().getExternalContext();
		ex.redirect("/home.xhtml");
	}
	
	/**
	 * Exclui um usu�rio
	 * @param usuario
	 */
	public void excluir(Usuario usuario){
		if(usuarioBusiness.excluir(usuario)){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Excluido com sucesso"));
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "", "Cilada Bino"));
		}
	}

	/*Get & Set*/
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
