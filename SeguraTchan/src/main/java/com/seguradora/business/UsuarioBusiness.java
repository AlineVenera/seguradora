package com.seguradora.business;

import java.util.List;

import com.seguradora.dao.UsuarioDao;
import com.seguradora.entidade.Usuario;

public class UsuarioBusiness {
	
	UsuarioDao usuarioDao = new UsuarioDao();
	
	/**
	 * Salva o usu�rio
	 * Se salvar retorna true, se n�o salvar retorna false
	 * @param usuario
	 * @return
	 */
	public boolean salvar(Usuario usuario){
		if(usuario == null){
			return false;
		}
		//Se der algum erro dentro do m�todo salvar, retorna false. Se correr tudo bem, suceeeeessooo
		return usuarioDao.salvarUsuario(usuario);
	}
	
	/**
	 * Validar o usu�rio pelo nome e senha
	 * @param nome
	 * @param senha
	 * @return
	 */
	public boolean validarUsuario(String nome, String senha) {
		return usuarioDao.validarUsuario(nome, senha);
	}
	
	/**
	 * Recupera uma lista com todos os usu�rios
	 * @return
	 */
	public List<Usuario> getUsuarios(){
		return usuarioDao.getUsuarios();
	}
	
	/**
	 * Exclui o usu�rio
	 * @param usuario
	 * @return
	 */
	public boolean excluir(Usuario usuario){
		if(usuario == null || (usuario != null && usuario.getId() == null)){
			return false;
		}
		return usuarioDao.excluir(usuario);
	}
	
}
