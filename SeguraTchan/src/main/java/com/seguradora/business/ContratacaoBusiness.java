package com.seguradora.business;

import com.seguradora.dao.ContratacaoDao;
import com.seguradora.entidade.Contratacao;

public class ContratacaoBusiness {
	
	/**
	 * Contratar
	 * @param contratacao
	 * @return
	 */
	public boolean contratar(Contratacao contratacao){
		ContratacaoDao contratacaoDao = new ContratacaoDao();
		return contratacaoDao.salva(contratacao);
	}
	
}
