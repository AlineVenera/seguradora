package com.seguradora.business;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import com.seguradora.dao.CotacaoDao;
import com.seguradora.dao.MarcaDao;
import com.seguradora.dao.PerfilDao;
import com.seguradora.dao.VeiculoDao;
import com.seguradora.entidade.Cotacao;
import com.seguradora.entidade.Marca;
import com.seguradora.entidade.Perfil;
import com.seguradora.entidade.Veiculo;
import com.seguradora.types.TipoResultadoCotacao;

public class CotacaoBusiness {
	
	/**
	 * M�todo para calcular a cota��o
	 * @param tipoResultadoCotacao, anoCarro, valorFipe
	 * @return
	 */
	public BigDecimal calculaCotacao(TipoResultadoCotacao tipoResultadoCotacao, int anoCarro, BigDecimal valorFipe){
		//Verifica se o carro tem mais de tr�s anos... quanto mais novo o carro, mais caro o seguro  
		boolean carroTemMaisTresAnos = (Calendar.getInstance().get(Calendar.YEAR) - anoCarro) >= 3 ;
		//Iniciando a vari�vel que recebe o valor da taxa
		BigDecimal taxaAplicada = new BigDecimal("0");
		
		switch (tipoResultadoCotacao) {
		//Seguro total: carros com mais de tr�s anos o seguro � igual a 5% do valor do carro, se n�o, � igual a 6% do valor do carro
		case TOTAL:
			if(carroTemMaisTresAnos){
				taxaAplicada = new BigDecimal(0.05);
			}
			taxaAplicada = new BigDecimal(0.06);
			break;
		//Seguro roubo e terceiro: carros com mais de tr�s anos o seguro � igual a 3% do valor do carro, se n�o, � igual a 4% do valor do carro
		case ROUBO_TERCEIRO:
			if(carroTemMaisTresAnos){
				taxaAplicada = new BigDecimal(0.03);
			}
			taxaAplicada = new BigDecimal(0.04);
			break;
		//Seguro terceiro: carros com mais de tr�s anos o seguro � igual a 1% do valor do carro, se n�o, � igual a 2% do valor do carro
		case TERCEIRO:
			if(carroTemMaisTresAnos){
				taxaAplicada = new BigDecimal(0.01);
			}
			taxaAplicada = new BigDecimal(0.02);
		break;
		default:
			break;
		}
		//m�ximo de dois d�gitos depois da v�rgula
		taxaAplicada = taxaAplicada.setScale(2, BigDecimal.ROUND_CEILING);
		//multiplica a taxa pelo valor da tabela fipe
		BigDecimal valorFinal = taxaAplicada.multiply(valorFipe);
		//m�ximo de dois d�gitos depois da v�rgula =)
		valorFinal = valorFinal.setScale(2, BigDecimal.ROUND_CEILING);
		return valorFinal;
	}
	
	/**
	 * Salvar a ve�culo
	 * @param veiculo
	 * @return
	 */
	public boolean salvar(Veiculo veiculo){
		VeiculoDao veiculoDao = new VeiculoDao();
		return veiculoDao.salvarVeiculo(veiculo);
	}
	
	/**
	 * Salvar a perfil
	 * @param perfil
	 * @return
	 */
	public boolean salvar(Perfil perfil){
		PerfilDao perfilDao = new PerfilDao();
		return perfilDao.salvaPerfil(perfil);
	}
	
	/**
	 * Salvar a cota��o
	 * @param cotacao
	 * @return
	 */
	public boolean salvar(Cotacao cotacao){
		CotacaoDao cotacaoDao = new CotacaoDao();
		return cotacaoDao.salva(cotacao);
	}
	
	/**
	 * Retorna uma lista com todas as marcas gravadas no banco
	 * @return
	 */
	public List<Marca> getMarcas(){
		MarcaDao marcaDao = new MarcaDao();
		return marcaDao.get();
	}
	
}
